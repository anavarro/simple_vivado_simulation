-- You can use this pkg to include all the definitions that would go on
-- other project packages or files, but that you don't want to include
-- directly because they import other packages which in turn import other
-- packages...

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

package frankenstein_pkg is

end package;
